<!-- 
    preg_match() : you can find value of parameter from strings 
    if it exists in string, then it returns 1(true), else returns 0(false).
    ex) you can find 'php' from next input sentence(string) -> you can see example below.
    'i' -> pattern modifier : you can see others from here -> http://www.php.net/manual/en/regexp.reference.delimiters.php
-->

<?php
  if (preg_match("/php/i", "PHP is the web scripting language of choice.")) {
      echo "A match was found.";
  } else {
      echo "A match was not found.";
  }
?>


